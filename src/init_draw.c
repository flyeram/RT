/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_draw.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 14:13:01 by tbalu             #+#    #+#             */
/*   Updated: 2016/05/25 14:19:01 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>
#include <stdlib.h>

/*
** function to initialize all the variable for each iteration
*/

void		initialize_draw(t_env *env, double x, double y)
{
	double		xx;
	double		yy;

	xx = (2 * ((double)x / (double)env->win_size.x) - 1) *
	env->win_ratio * ZOOM;
	yy = (1 - 2 * ((double)y / (double)env->win_size.y)) * ZOOM;
	env->data_scene->ray->dir.x = xx * cos(env->yaw) * cos(env->pitch) +
	yy * (cos(env->yaw) * sin(env->pitch) * sin(env->roll) - sin(env->yaw) *
	cos(env->roll)) + cos(env->yaw) * sin(env->pitch) * cos(env->roll) +
	sin(env->yaw) * sin(env->roll);
	env->data_scene->ray->dir.y = xx * sin(env->yaw) * cos(env->pitch) + yy *
	(sin(env->yaw) * sin(env->pitch) * sin(env->roll) + cos(env->yaw) *
	cos(env->roll)) + sin(env->yaw) * sin(env->pitch) * cos(env->roll) -
	cos(env->yaw) * sin(env->roll);
	env->data_scene->ray->dir.z = xx * -sin(env->pitch) + yy * cos(env->pitch)
	* sin(env->roll) + cos(env->pitch) * cos(env->roll);
	env->data_scene->ray->dir = normalize(&env->data_scene->ray->dir);
	env->data_scene->ray->start = env->camera->origin;
	env->data_scene->level = 0;
}

int			initialize_data_scene(t_env *env)
{
	t_data		*data;
	t_ray		*ray;

	if (!(ray = (t_ray *)malloc(sizeof(t_ray))))
		return (-1);
	ray->start = create_vector(0.0, 0.0, 0.0);
	ray->dir = create_vector(0.0, 0.0, 0.0);
	if (!(data = (t_data *)malloc(sizeof(t_data))))
		return (-1);
	data->ray = ray;
	data->color_final = color_percent(0, 0, 0);
	env->data_scene = data;
	return (1);
}
