/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:46:20 by tbalu             #+#    #+#             */
/*   Updated: 2016/05/12 13:33:30 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <libft.h>
#include <unistd.h>
#include <rt.h>
#include <stdlib.h>

void		create_functions(t_env *env)
{
	env->functions[0] = &intersect_sphere;
	env->functions[1] = &intersect_plane;
	env->functions[2] = &intersect_cylinder;
	env->functions[3] = &intersect_cone;
	env->functions[4] = &intersect_disk;
	env->n_fct[0] = &find_normal_sphere;
	env->n_fct[1] = &find_normal_plane;
	env->n_fct[2] = &find_normal_cylinder;
	env->n_fct[3] = &find_normal_cone;
	env->n_fct[4] = &find_normal_plane;
}

/*
**function which initialize the events and start the loop
*/

static int		init_env(t_env *env)
{
	if (!((*env).mlx = mlx_init()))
		return (0);
	(*env).win = mlx_new_window((*env).mlx, WIN_X, WIN_Y, "rt");
	(*env).image = create_image((*env).mlx, WIN_X, WIN_Y);
	(*env).win_size.x = WIN_X;
	(*env).win_size.y = WIN_Y;
	(*env).scene = NULL;
	(*env).data_scene = NULL;
	if (!((*env).camera = (t_camera *)malloc(sizeof(t_camera))))
		return (0);
	(*env).pitch = 0.0f;
	(*env).yaw = 3.150f;
	(*env).roll = 0.0;
	(*env).aliasing = 0;
	return (1);
}

t_env			*genv(void)
{
	static t_env env;

	return (&env);
}

void			executor(t_env *env)
{
	draw_loop(env);
	mlx_expose_hook((*env).win, expose, env);
	mlx_hook((*env).win, 2, (1L << 0), press_key, env);
	mlx_loop((*env).mlx);
}

int			main(int ac, char **av)
{
	t_env	*env;

	if (ac != 2)
		return (0);
	init_env(genv());
	env = genv();
	env->camera->origin = create_vector(500, -100.0f, -1000.0f);
	env->scene = constructor_scene(av[1]);
	create_functions(env);
	env->win_ratio = (double)env->win_size.x / (double)env->win_size.y;
	if (env->scene == NULL)
		return (0);
	executor(env);
	return (0);
}
