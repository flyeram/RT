/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   anti_aliasing.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 11:56:06 by tbalu             #+#    #+#             */
/*   Updated: 2016/05/30 14:47:01 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <stdlib.h>
#include <math.h>

t_color		*color_average(int *color_pixels)
{
	t_color *color_alias;
	int		i;

	color_alias = (t_color *)malloc(sizeof(t_color));
	color_alias->r = 0.0;
	color_alias->g = 0.0;
	color_alias->b = 0.0;
	i = 0;
	while (i < 81)
	{
		color_alias->r += (color_pixels[i] >> 16) & 0xFF;
		color_alias->g += (color_pixels[i] >> 8) & 0xFF;
		color_alias->b += (color_pixels[i] >> 0) & 0xFF;
		i++;
	}
	color_alias->r /= 81;
	color_alias->g /= 81;
	color_alias->b /= 81;
	return (color_alias);
}

t_color		*get_color_alias(t_env *env, int x, int y)
{
	int		i;
	int		j;
	int		*color_pixels;

	color_pixels = (int *)malloc(81 * sizeof(int));
	i = 0;
	while (i < 9)
	{
		j = 0;
		while (j < 9)
		{
			color_pixels[9 * i + j] = get_color(env->image,
				x - 4 + i, y - 4 + j);
			j++;
		}
		i++;
	}
	return (color_average(color_pixels));
}

/*t_color		*color_average(int *color_pixels)
{
	t_color *color_alias;
	int		i;

	color_alias = (t_color *)malloc(sizeof(t_color));
	color_alias->r = 0.0;
	color_alias->g = 0.0;
	color_alias->b = 0.0;
	i = 0;
	while (i < 81)
	{
		color_alias->r += (color_pixels[i] >> 16) & 0xFF;
		color_alias->g += (color_pixels[i] >> 8) & 0xFF;
		color_alias->b += (color_pixels[i] >> 0) & 0xFF;
		i++;
	}
	color_alias->r /= 81;
	color_alias->g /= 81;
	color_alias->b /= 81;
	return (color_alias);
}

t_color		*get_color_alias(t_env *env, int x, int y)
{
	int		*color_pixels;
	int		i;
	int		j;

	color_pixels = (int *)malloc(81 * sizeof(int));
	i = 0;
	while (i < 9)
	{
		j = 0;
		while (j < 9)
		{
			color_pixels[i / 9 + j % 9] = get_color(env->image, x - 4 + i, y - 4 + j);
			j++;
		}
		i++;
	}
	return (color_average(color_pixels));
}*/

void		anti_aliasing(t_env *env)
{
	int		x;
	int		y;
	t_color	*color_alias;

	y = 0;
	while (y < env->win_size.y)
	{
		x = 0;
		while (x < env->win_size.x)
		{
			color_alias = get_color_alias(env, x, y);
			image_put_pixel(*env, x, y, create_color(0,
			ft_min(color_alias->r, 255.0f),
			ft_min(color_alias->g, 255.0f),
			ft_min(color_alias->b, 255.0f)));
			x++;
		}
		y++;
	}
}
