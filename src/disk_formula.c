/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   disk_formula.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 14:37:03 by tbalu             #+#    #+#             */
/*   Updated: 2016/05/30 17:03:10 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>

int		intersect_disk(t_obj *disk, t_ray *ray, double *t)
{
	t_vector	scale;
	t_vector	intersect_point;
	t_vector	diff;
	double		dot;
	double		prev_t;

	prev_t = *t;
	if (intersect_plane(disk, ray, t) == 0)
	{
		scale = vector_scale(&ray->dir, *t);
		intersect_point = add_vector(&scale, &ray->start);
		diff = substract_vector(&intersect_point, &disk->pos);
		dot = dot_product(&diff, &diff);
		if (dot <= POW2(disk->radius))
			return (0);
		else
		{
			*t = prev_t;
			return (-1);
		}
	}
	else
		return (-1);
}
