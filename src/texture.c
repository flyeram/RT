/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/11 11:49:20 by tbalu             #+#    #+#             */
/*   Updated: 2016/05/30 16:49:01 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>
#include <libft.h>
#include <mlx.h>

double		get_u(t_obj *obj, double phi, t_vector *north, t_vector *ray_point)
{
	t_vector	equa;
	t_vector	cross;
	double		theta;
	double		u;

	equa = create_vector(obj->radius, 0, 0);
	equa = normalize(&equa);
	theta = (acos(dot_product(ray_point, &equa) / sin(phi))) / (2 * PI);
	cross = cross_product(north, &equa);
	if (dot_product(&cross, ray_point) > 0)
		u = theta;
	else
		u = 1 - theta;
	return (u);
}

t_vector	get_uv(int current)
{
	t_vector	ray_point;
	t_vector	north;
	t_obj		*obj;
	double		phi;
	t_vector	uv;

	obj = genv()->scene->obj_tab[current];
	north = create_vector(0, obj->radius, 0);
	north = normalize(&north);
	ray_point = substract_vector(&genv()->data_scene->new_start, &obj->pos);
	ray_point = normalize(&ray_point);
	phi = acos(-dot_product(&north, &ray_point));
	uv.y = phi / PI;
	uv.x = get_u(obj, phi, &north, &ray_point);
	return (uv);
}

t_color		get_pixel_texture(int current, t_image *texture, t_color *diffuse)
{
	t_vector	uv;
	t_color		pixel;

	uv = get_uv(current);
	uv.x *= texture->width * 1;
	uv.y *= texture->height * 1;
	pixel = itorgb(get_color(texture, uv.x, uv.y));
	pixel.r = (pixel.r / 255) * diffuse->r;
	pixel.g = (pixel.g / 255) * diffuse->g;
	pixel.b = (pixel.b / 255) * diffuse->b;
	return (pixel);
}

/*t_color		get_pixel_texture(int current, t_image *texture, t_color *diffuse)
{
	double		val_1;
	double		val_2;
	t_color		pixel;
	t_obj		*obj;

	obj = genv()->scene->obj_tab[current];
	texture->width += 0;
	diffuse->b += 0;
	if (obj->normale.x != 0)
	{
		val_1 = genv()->data_scene->new_start.y;
		val_2 = genv()->data_scene->new_start.z;
	}
	else if (obj->normale.y != 0)
	{
		val_1 = genv()->data_scene->new_start.x;
		val_2 = genv()->data_scene->new_start.z;
	}
	else
	{
		val_1 = genv()->data_scene->new_start.x;
		val_2 = genv()->data_scene->new_start.y;
	}
	if (((int)(ceil(val_1 / 10)) + (int)(ceil(val_2 / 10))) % 2 == 0)
	{
		pixel = *diffuse;
	}
	else
	{
		pixel.r = diffuse->r + 0.3;
		pixel.g = diffuse->g + 0.3;
		pixel.b = diffuse->b + 0.3;
	}
	return (pixel);
}*/
