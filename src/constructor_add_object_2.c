/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   constructor_add_object_2.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 14:28:17 by tbalu             #+#    #+#             */
/*   Updated: 2016/04/28 14:50:44 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <rt.h>
#include <stdlib.h>

int			constructor_add_disk(char **line_split, t_scene *scene,
			t_tmp *obj_val)
{
	int		nb;

	if (ft_arraylen(line_split) != 8)
		return (-1);
	scene->obj_count++;
	nb = scene->obj_count;
	if (nb >= obj_val->obj_tmp_count)
		return (-1);
	if (!(scene->obj_tab[nb] = (t_obj *)malloc(sizeof(t_obj))))
		return (-1);
	scene->obj_tab[nb]->pos = create_vector(
	ft_stof(line_split[0]), ft_stof(line_split[1]), ft_stof(line_split[2]));
	scene->obj_tab[nb]->normale = create_vector(
	ft_stof(line_split[3]), ft_stof(line_split[4]), ft_stof(line_split[5]));
	scene->obj_tab[nb]->normale = normalize(&scene->obj_tab[nb]->normale);
	scene->obj_tab[nb]->radius = ft_stof(line_split[6]);
	scene->obj_tab[nb]->material = ft_atoi(line_split[7]);
	if (scene->obj_tab[nb]->material >= obj_val->material_tmp_count
	|| scene->obj_tab[nb]->material < 0)
		return (-1);
	scene->obj_tab[nb]->type = 4;
	return (1);
}
