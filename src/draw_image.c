/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_image.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/02 18:51:46 by tbalu             #+#    #+#             */
/*   Updated: 2016/05/30 16:49:41 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <stdlib.h>
#include <libft.h>
#include <math.h>
#include <mlx.h>


/*
**function where we check if the ray hit an object or not
*/

int			check_object(t_env *env, t_ray *ray)
{
	int		i;
	int		current_object;

	i = 0;
	current_object = -1;
	while (i < env->scene->obj_count)
	{
		if (env->functions[env->scene->obj_tab[i]->type]
(env->scene->obj_tab[i], ray, &(env->data_scene->t)) == 0)
			current_object = i;
		i++;
	}
	return (current_object);
}

/*
**function which launch all the other function like shadow, light and object
*/

void		draw_each(t_env *env)
{
	int			cur;
	double		reflect;
	t_vector	tmp;

	while ((env->data_scene->coef > 0.0f && env->data_scene->level < 2))
	{
		env->data_scene->t = 20000.0f;
		if ((cur = check_object(env, env->data_scene->ray)) == -1)
			return ;
		if ((env->n_fct[env->scene->obj_tab[cur]->type](env, cur)) == -1)
			return ;
		light_function(env, cur);
		env->data_scene->coef *=
		env->scene->material_tab[env->scene->obj_tab[cur]->material]->ref;
		env->data_scene->ray->start = env->data_scene->new_start;
		reflect = 2.0f * dot_product(&env->data_scene->ray->dir,
		&env->data_scene->normal);
		tmp = vector_scale(&env->data_scene->normal, reflect);
		env->data_scene->ray->dir =
		substract_vector(&env->data_scene->ray->dir, &tmp);
		env->data_scene->level++;
	}
}

void		draw_antialiased(t_env *env, int x, int y)
{
	double		fragx;
	double		fragy;

	env->data_scene->color_final.r = 0.0;
	env->data_scene->color_final.g = 0.0;
	env->data_scene->color_final.b = 0.0;
	fragx = x;
	while (fragx < x + 1)
	{
		fragy = y;
		while (fragy < y + 1)
		{
			env->data_scene->coef = 0.25;
			initialize_draw(env, fragx, fragy);
			draw_each(env);
			fragy += 0.5;
		}
		fragx += 0.5;
	}
}

void		draw_aliased(t_env *env, int x, int y)
{
	env->data_scene->color_final.r = 0.0;
	env->data_scene->color_final.g = 0.0;
	env->data_scene->color_final.b = 0.0;
	env->data_scene->coef = 1.0;
	initialize_draw(env, (double)x, (double)y);
	draw_each(env);
}

/*
**main function to draw, initialize variables and launch the loop for each pixel
*/

void		draw_loop(t_env *env)
{
	int		x;
	int		y;

	if ((initialize_data_scene(env)) == -1)
		return ;
	y = 0;
	while (y < env->win_size.y)
	{
		x = 0;
		while (x < env->win_size.x)
		{
			if (env->aliasing == 1)
				draw_antialiased(env, x, y);
			else
				draw_aliased(env, x, y);
			image_put_pixel(*env, x, y, create_color(0,
			ft_min(env->data_scene->color_final.r * 255.0f, 255.0f),
			ft_min(env->data_scene->color_final.g * 255.0f, 255.0f),
			ft_min(env->data_scene->color_final.b * 255.0f, 255.0f)));
			x++;
		}
		/*mlx_put_image_to_window(env->mlx, env->win, env->image->img, 0, 0);
		mlx_string_put(env->mlx, env->win, 0, 0, 0xFFFFFF, ft_itoa((int)((y / env->win_size.y) * 100)));*/
		y++;
	}
	//anti_aliasing(env);
}
