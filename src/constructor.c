/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   constructor.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/23 13:01:05 by tbalu             #+#    #+#             */
/*   Updated: 2016/05/12 13:32:14 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <mlx.h>
#include <stdlib.h>
#include <fcntl.h>

/*
**function which launch the good function to create the proper object
*/

int			constructor_add_object(char *line, t_scene *scene, t_tmp *obj_val)
{
	char		**line_split;
	int			return_value;

	return_value = 0;
	line_split = ft_strsplit(line, ' ');
	if (ft_strcmp(line_split[0], "sphere") == 0)
		return_value = constructor_add_sphere(&line_split[1], scene, obj_val);
	else if (ft_strcmp(line_split[0], "plane") == 0)
		return_value = constructor_add_plane(&line_split[1], scene, obj_val);
	else if (ft_strcmp(line_split[0], "cylinder") == 0)
		return_value = constructor_add_cylinder(&line_split[1], scene,
		obj_val, 2);
	else if (ft_strcmp(line_split[0], "light") == 0)
		return_value = constructor_add_light(&line_split[1], scene, obj_val);
	else if (ft_strcmp(line_split[0], "material") == 0)
		return_value = constructor_add_material(&line_split[1], scene, obj_val);
	else if (ft_strcmp(line_split[0], "cone") == 0)
		return_value = constructor_add_cylinder(&line_split[1], scene,
		obj_val, 3);
	else if (ft_strcmp(line_split[0], "count") == 0)
		return_value = constructor_add_count(&line_split[1], scene, obj_val);
	else if (ft_strcmp(line_split[0], "disk") == 0)
		return_value = constructor_add_disk(&line_split[1], scene, obj_val);
	ft_free_array(line_split);
	return (return_value);
}

/*
**function which allocate the good amount of memory for the array of object
**return (1 or -1) if the allocation success or fail
*/

int			constructor_add_count(char **line_split, t_scene *scene,
t_tmp *obj_val)
{
	if (ft_arraylen(line_split) != 3)
		return (-1);
	if (!(scene->material_tab = (t_material **)malloc(sizeof(t_material *)
		* ft_atoi(line_split[0]))))
		return (-1);
	if (!(scene->light_tab = (t_light **)malloc(sizeof(t_light *)
		* ft_atoi(line_split[1]))))
		return (-1);
	if (!(scene->obj_tab = (t_obj **)malloc(sizeof(t_obj *)
		* ft_atoi(line_split[2]))))
		return (-1);
	obj_val->material_tmp_count = ft_atoi(line_split[0]);
	obj_val->light_tmp_count = ft_atoi(line_split[1]);
	obj_val->obj_tmp_count = ft_atoi(line_split[2]);
	scene->obj_count = -1;
	scene->light_count = -1;
	scene->material_count = -1;
	return (1);
}

int			check_number(t_scene *scene, t_tmp *obj_val)
{
	scene->obj_count++;
	scene->light_count++;
	scene->material_count++;
	if (scene->obj_count != obj_val->obj_tmp_count)
		return (-1);
	if (scene->light_count != obj_val->light_tmp_count)
		return (-1);
	if (scene->material_count != obj_val->material_tmp_count)
		return (-1);
	return (0);
}

/*
**function which take a filename and read it to create a new scene and return it
*/

t_scene		*constructor_scene(char *file)
{
	int		fd;
	t_scene	*scene;
	char	*line;
	int		return_value;
	t_tmp	*obj_val;

	if (!(obj_val = (t_tmp *)malloc(sizeof(t_tmp)))
	|| !(scene = (t_scene *)malloc(sizeof(t_scene))))
		return (NULL);
	ft_bzero(obj_val, sizeof(t_tmp));
	ft_bzero(scene, sizeof(t_scene));
	if ((fd = open(file, O_RDONLY)) == -1)
		return (NULL);
	while ((return_value = get_next_line(fd, &line)))
	{
		if (return_value == -1)
			return (NULL);
		if (constructor_add_object(line, scene, obj_val) == -1)
			return (NULL);
	}
	if (check_number(scene, obj_val) == -1)
		return (NULL);
	free(obj_val);
	return (scene);
}
