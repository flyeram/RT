/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sphere_formula.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/02 18:20:11 by tbalu             #+#    #+#             */
/*   Updated: 2016/02/25 13:09:41 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>
#include <libft.h>

/*
**a function to know if there is intersection between the ray and the sphere
**return 1 if yes and 0 if no
*/

int			find_normal_sphere(t_env *env, int current)
{
	t_vector	scale;
	double		temp;

	scale = vector_scale(&env->data_scene->ray->dir, env->data_scene->t);
	env->data_scene->new_start = add_vector(&scale,
	&env->data_scene->ray->start);
	env->data_scene->normal = substract_vector(&env->data_scene->new_start,
	&env->scene->obj_tab[current]->pos);
	if ((temp = dot_product(&env->data_scene->normal,
	&env->data_scene->normal)) == 0)
		return (-1);
	temp = 1.0f / sqrt(temp);
	env->data_scene->normal = vector_scale(&env->data_scene->normal, temp);
	return (0);
}

int			intersect_sphere(t_obj *sphere, t_ray *ray, double *t)
{
	t_vector	dist;
	double		abc[3];
	double		discr;

	abc[0] = dot_product(&ray->dir, &ray->dir);
	dist = substract_vector(&ray->start, &sphere->pos);
	abc[1] = 2 * dot_product(&dist, &ray->dir);
	abc[2] = dot_product(&dist, &dist) - (sphere->radius * sphere->radius);
	discr = abc[1] * abc[1] - (4 * abc[0] * abc[2]);
	if (discr < 0)
		return (-1);
	else
		return (resolv_quadratic(discr, t, abc[1], abc[0]));
}
