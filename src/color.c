/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 12:35:34 by tbalu             #+#    #+#             */
/*   Updated: 2016/05/11 16:47:43 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <stdlib.h>

/*
**function which take alpha, (red, green, blue) values between 0-255 and return
**a color which corrspond to the mix of rgb;
*/

unsigned int	create_color(int a, int r, int g, int b)
{
	return (a << 24 | r << 16 | g << 8 | b);
}

t_color			color_percent(double r, double g, double b)
{
	t_color		new;

	new.r = r;
	new.g = g;
	new.b = b;
	return (new);
}

t_color			itorgb(unsigned int color)
{
	return (color_percent((color >> 16) & 0xFF,
		(color >> 8) & 0xFF, (color >> 0) & 0xFF));
}
