/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/04 17:57:52 by tbalu             #+#    #+#             */
/*   Updated: 2016/05/25 14:08:42 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <stdlib.h>
#include <math.h>
#include <libft.h>

int			is_in_shadow(t_env *env, t_ray *light, int i_l)
{
	int		i;
	int		in_shadow;
	double	dist;

	i = 0;
	in_shadow = 0;
	env->data_scene->t = 20000.0f;
	while (i < env->scene->obj_count)
	{
		if (env->functions[env->scene->obj_tab[i]->type]
(env->scene->obj_tab[i], light, &(env->data_scene->t)) == 0)
		{
			dist = dist_vector(&light->start,
			&env->scene->light_tab[i_l]->pos);
			if (env->data_scene->t < dist)
			{
				in_shadow = 1;
				break ;
			}
		}
		i++;
	}
	return (in_shadow);
}

void		specular(t_env *env, int i, t_ray *light)
{
	t_vector	dir;
	double		factor;

	dir = vector_scale(&env->data_scene->normal,
	2 * dot_product(&env->data_scene->normal, &light->dir));
	dir = substract_vector(&dir, &light->dir);
	factor = pow(dot_product(&dir, &env->data_scene->ray->dir), 20);
	env->data_scene->color_final.r += env->scene->light_tab[i]->intensity.r *
	0.6 * factor * env->data_scene->coef;
	env->data_scene->color_final.g += env->scene->light_tab[i]->intensity.g *
	0.6 * factor * env->data_scene->coef;
	env->data_scene->color_final.b += env->scene->light_tab[i]->intensity.b *
	0.6 * factor * env->data_scene->coef;
}

void		lamber_diffusion(t_env *env, int i, int current, t_ray *light)
{
	double		lambert;
	t_material	*mat;
	int			in_shadow;
	t_color		p_color;

	in_shadow = is_in_shadow(env, light, i);
	if ((in_shadow) == 1)
		return ;
	specular(env, i, light);
	mat = env->scene->material_tab[env->scene->obj_tab[current]->material];
	if (mat->texture == NULL)
		p_color = mat->diffuse;
	else
		p_color = get_pixel_texture(current, mat->texture, &mat->diffuse);
	lambert = dot_product(&light->dir, &env->data_scene->normal) *
	env->data_scene->coef;
	env->data_scene->color_final.r += env->scene->light_tab[i]->intensity.r *
	p_color.r * lambert;
	env->data_scene->color_final.g += env->scene->light_tab[i]->intensity.g *
	p_color.g * lambert;
	env->data_scene->color_final.b += env->scene->light_tab[i]->intensity.b *
	p_color.b * lambert;
}

void		ambient_light(t_env *env, int i, int current)
{
	t_material	*mat;
	t_color		p_color;

	mat = env->scene->material_tab[env->scene->obj_tab[current]->material];
	if (mat->texture == NULL)
		p_color = mat->diffuse;
	else
		p_color = get_pixel_texture(current, mat->texture, &mat->diffuse);
	env->data_scene->color_final.r += env->scene->light_tab[i]->intensity.r *
	p_color.r * 0.2 * env->data_scene->coef;
	env->data_scene->color_final.g += env->scene->light_tab[i]->intensity.g *
	p_color.g * 0.2 * env->data_scene->coef;
	env->data_scene->color_final.b += env->scene->light_tab[i]->intensity.b *
	p_color.b * 0.2 * env->data_scene->coef;
}

void		light_function(t_env *env, int current)
{
	int			i;
	t_vector	dist;
	double		t;
	t_ray		light;

	i = 0;
	while (i < env->scene->light_count)
	{
		ambient_light(env, i, current);
		dist = substract_vector(&env->scene->light_tab[i]->pos,
		&env->data_scene->new_start);
		if (dot_product(&env->data_scene->normal, &dist) <= 0.001f ||
			(t = sqrt(dot_product(&dist, &dist))) <= 0.001f)
		{
			i++;
			continue ;
		}
		light.start = env->data_scene->new_start;
		light.dir = vector_scale(&dist, 1 / t);
		lamber_diffusion(env, i, current, &light);
		i++;
	}
}
