/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   plane_formula.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 12:02:53 by tbalu             #+#    #+#             */
/*   Updated: 2016/02/26 11:13:04 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>

/*
**function to know if there is an intersection between a plane and the ray
*/

int		find_normal_plane(t_env *env, int current)
{
	t_obj		*plane;
	double		dot;
	t_vector	scale;

	scale = vector_scale(&env->data_scene->ray->dir, env->data_scene->t);
	env->data_scene->new_start = add_vector(&scale,
	&env->data_scene->ray->start);
	plane = env->scene->obj_tab[current];
	dot = dot_product(&env->data_scene->ray->dir, &plane->normale);
	if (dot < 0.0001f)
		env->data_scene->normal = vector_scale(&plane->normale, 1.00f);
	else
		env->data_scene->normal = vector_scale(&plane->normale, -1.00f);
	return (0);
}

int		intersect_plane(t_obj *plane, t_ray *ray, double *t)
{
	t_vector	diff;
	double		tmp;
	double		scale_1;
	double		scale_2;

	diff = substract_vector(&ray->start, &plane->pos);
	diff = vector_scale(&diff, -1);
	scale_1 = dot_product(&plane->normale, &diff);
	scale_2 = dot_product(&plane->normale, &ray->dir);
	if (scale_2 == 0)
		return (-1);
	tmp = scale_1 / scale_2;
	if (tmp >= 0.001f && tmp < *t)
	{
		*t = tmp;
		return (0);
	}
	return (-1);
}
