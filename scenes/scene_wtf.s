count 7 3 15
sphere 200 300 200 150 0
sphere 300 300 200 150 1
sphere 400 300 200 150 2
sphere 500 300 200 150 0
sphere 600 300 200 150 1
sphere 700 300 200 150 2
sphere 900 300 200 150 0
sphere 950 250 0 50 2
sphere 950 380 0 50 6
plane 300 400 600 0 0 1 3
plane 300 400 200 0 1 0 4
plane 300 -500 200 0 1 0 4
disk 0 300 -600 0 0 1 100 6
cylinder -600 100 150 0 1 0 100 5
cone 1400 200 100 0 1 0 30 6
light 3000 -400 -4000 0.5 0.5 0.5
light -2200 300 -4000 0.5 0.5 0.5
light -1000 300 -4000 0.5 0.5 0.5
material 1 1 1 0 textures/greendragon.xpm
material 0 1 0 0.4
material 0 0 1 0.8
material 0.5 0.5 0 0.0
material 0.5 1 0.5 0.0
material 0.5 0 0 0.3
material 0 0.3 0 1
